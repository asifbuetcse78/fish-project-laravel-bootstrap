<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Importer;
use App\Fisherman;
use Carbon\Carbon;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function parseExcel(Request $request) 
    {
        $allFisherManInfo = [];
        $excel = Importer::make('Excel');
        $excel->load($request->file('fishermanList')->getPathName());
        $collection = $excel->getCollection();
        $flipped = array_flip($collection[0]);
        for($i = 1; $i < count($collection); $i++) {
            $tempFisherMan = new Fisherman();
            $tempFisherMan->name_in_bangla = $collection[$i][$flipped['নাম']];
            $tempFisherMan->name_in_english = $collection[$i][$flipped["নাম (ইংরেজীতে)"]];

            $tempFisherMan->nid = $collection[$i][$flipped["জাতীয় পরিচয়পত্র নং"]];
            $tempFisherMan->fathers_name_in_bangla = $collection[$i][$flipped["পিতার নাম"]];
            $tempFisherMan->mothers_name_in_bangla = $collection[$i][$flipped["পিতার নাম"]];
            $tempFisherMan->birthday = $collection[$i][$flipped["জন্ম তারিখ"]];
            $tempFisherMan->religion = $collection[$i][$flipped["ধর্ম"]];
            $tempFisherMan->education = $collection[$i][$flipped["শিক্ষগত যোগ্যতা"]];
            $tempFisherMan->marital_status = $collection[$i][$flipped["বৈবাহিক অবস্থা"]];
            $tempFisherMan->number_of_family_members = $collection[$i][$flipped["পরিবারের সদস্য সংখ্যা"]];
            $tempFisherMan->village_in_bangla = $collection[$i][$flipped["গ্রাম"]];
            $tempFisherMan->postoffice_in_bangla = $collection[$i][$flipped["ডাকঘর"]];
            $tempFisherMan->ward_number = is_int($collection[$i][$flipped["ওয়ার্ড নং"]])? $collection[$i][$flipped["ওয়ার্ড নং"]]: -1;
            $tempFisherMan->union_in_bangla = $collection[$i][$flipped["ইউনিয়ন"]];
            $tempFisherMan->mobile_number = $collection[$i][$flipped["মোবাইল নং"]];
            $tempFisherMan->main_occupation = $collection[$i][$flipped["প্রধান পেশা"]];
            $tempFisherMan->secondary_occupation = $collection[$i][$flipped["সহকারী পেশা"]];
            $tempFisherMan->place_to_collect_fish = $collection[$i][$flipped["মৎস্য আহরণ স্থল"]];
            $tempFisherMan->time_to_collect_fish = $collection[$i][$flipped["মৎস্য আহরণ কাল"]];
            $tempFisherMan->nature_of_collected_fish = $collection[$i][$flipped["আহরিত মাছের ধরণ"]];
            $tempFisherMan->tools_to_collect_fish = $collection[$i][$flipped["ব্যবহৃত উপকরণ"]];
            $tempFisherMan->annual_income = is_int($collection[$i][$flipped["বার্ষিক আয়"]]) ? $collection[$i][$flipped["বার্ষিক আয়"]]: 0;
            $tempFisherMan->category = $collection[$i][$flipped["ক্যাটাগরি"]];
            $tempFisherMan->has_personal_boat =isset($collection[$i][$flipped["ব্যক্তিগত নৌযান আছে কিনা"]]) ? ($collection[$i][$flipped["ব্যক্তিগত নৌযান আছে কিনা"]] == "না" ? 0 : 1) : null;
            $tempFisherMan->personal_boat_name = isset($collection[$i][$flipped["নৌযানের নাম"]]) ? $collection[$i][$flipped["নৌযানের নাম"]] : null;
            $tempFisherMan->power_of_engine = isset($collection[$i][$flipped["ইঞ্জিনের ক্ষমতা"]]) ? $collection[$i][$flipped["ইঞ্জিনের ক্ষমতা"]] : null;
            $tempFisherMan->personal_boat_length = isset($collection[$i][$flipped["নৈাযানের দৈর্ঘ্য"]]) ? $collection[$i][$flipped["নৈাযানের দৈর্ঘ্য"]] : null;
            $tempFisherMan->save();
            array_push($allFisherManInfo, $tempFisherMan);
        }
        return redirect()->route('home');
    }
}
