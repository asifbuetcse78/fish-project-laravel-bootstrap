<?php

namespace App\Http\Controllers;

use App\Fisherman;
use Illuminate\Http\Request;

class FishermanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fisherman.index', ['fishermen' => Fisherman::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fisherman  $fisherman
     * @return \Illuminate\Http\Response
     */
    public function show(Fisherman $fisherman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fisherman  $fisherman
     * @return \Illuminate\Http\Response
     */
    public function edit(Fisherman $fisherman)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fisherman  $fisherman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fisherman $fisherman)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fisherman  $fisherman
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fisherman $fisherman)
    {
        //
    }
}
