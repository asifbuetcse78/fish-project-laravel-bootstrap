<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fisherman extends Model
{
    public $timestamps = false;
}
