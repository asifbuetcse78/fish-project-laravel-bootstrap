<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFishermenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fishermen', function (Blueprint $table) {
            $table->id();
            $table->string('name_in_bangla')->nullable();
            $table->string('name_in_english')->nullable();
            $table->string('nid')->nullable();
            $table->string('fathers_name_in_bangla')->nullable();
            $table->string('mothers_name_in_bangla')->nullable();
            $table->string('birthday')->nullable();
            $table->string('religion')->nullable();
            $table->string('education')->nullable();
            $table->string('marital_status')->nullable();
            $table->string('number_of_family_members')->nullable();
            $table->string('village_in_bangla')->nullable();
            $table->string('postoffice_in_bangla')->nullable();
            $table->integer('ward_number')->nullable();
            $table->string('union_in_bangla')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('main_occupation')->nullable();
            $table->string('secondary_occupation')->nullable();
            $table->string('place_to_collect_fish')->nullable();
            $table->string('time_to_collect_fish')->nullable();
            $table->string('nature_of_collected_fish')->nullable();
            $table->string('tools_to_collect_fish')->nullable();
            $table->integer('annual_income')->nullable();
            $table->string('category')->nullable();
            $table->boolean('has_personal_boat')->nullable();
            $table->string('personal_boat_name')->nullable();
            $table->string('power_of_engine')->nullable();
            $table->string('personal_boat_length')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fishermen');
    }
}
