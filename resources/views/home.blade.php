@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
      <div class="card home-card">
        <div class="card-header">{{ __('এডমিন প্যানেল') }}</div>

        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif
          <form method="post" action="/uploadexcel" enctype="multipart/form-data">
            {{ csrf_field() }}
            এক্সেল থেকে ডাটা লোড<br/><br/>
            <div class="custom-file mb-3">
              <input type="file" class="custom-file-input" id="fishermanList" name="fishermanList" onchange="onChangeCustomFileUplad()">
              <label class="custom-file-label" for="fishermanList">ফাইল বাছুন</label>
            </div>
            <button type="submit" class="btn btn-outline-info">লোড করুন</button>
          </form>
          <br/>
          <hr/>
          <div>
            সব জেলেদের তালিকাঃ<br/><br/>
            <a href="{{ url('/fishermen') }}" class="btn btn-xs btn-outline-info pull-right">তালিকা দেখান</a>

            <!-- <button class="btn btn-primary">Show List</button> -->
          </div>
        </div>
      </div>
  </div>
</div>
@endsection
