@extends('layouts.app')

@section('content')
<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>নাম</th>
            <th>বাবার নাম</th>
            <th>জাতীয় পরিচয়পত্র</th>
            <th>পরিবারে সদস্য</th>
            <th>গ্রাম</th>
            <th>ডাকঘর</th>
            <th>ইউনিয়ন</th>
            <th>বার্ষিক আয়</th>
        </tr>
    </thead>
    <tbody>
    	@foreach($fishermen as $fisherman)
        <tr>
            <td>{{ $fisherman->name_in_bangla }}</td>         
            <td>{{ $fisherman->fathers_name_in_bangla }}</td>
            <td>{{ $fisherman->nid }}</td> 
            <td>{{ $fisherman->number_of_family_members }}</td> 
            <td>{{ $fisherman->village_in_bangla }}</td> 
            <td>{{ $fisherman->postoffice_in_bangla }}</td> 
            <td>{{ $fisherman->union_in_bangla }}</td> 
            <td>{{ $fisherman->annual_income }}</td>            
        </tr>
        @endforeach
    </tbody>
</table>

@endsection